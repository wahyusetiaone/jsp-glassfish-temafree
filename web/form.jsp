<%-- 
    Document   : form
    Created on : Mar 27, 2019, 3:50:57 AM
    Author     : abah
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="css/cssku.css"> 
</head>
<body>
    <header>
        <img src="image/wordmark4.png" alt="none">
    </header>
    <nav class="menu">
        <aside class="daftar-menu">
            <ul>
                <li><a href="/WA-GlassFishServer/">Home</a></li>
                <li><a href="/WA-GlassFishServer/form.jsp">Form</a></li>
                <li><a href="#contact">Contact</a></li>
                <li><a href="#about">About</a></li>
            </ul>              
        </aside>
    </nav>
    <section class="isi">
        <article class="konten">
            <div class="kiri">
                <h2>Halaman Judul</h2>
                <div class="tengah-medium">
                    <form method="post" action="hasil-form.jsp">
                    <label for="fobat">Nama Obat</label>
                    <input type="text" id="fobat" name="fobat" placeholder="Nama Obat..">
                    <label for="ftgl">Tgl Pembuatan <span class="note"> Tanggal Max 10 Maret 2019 </span></label>
                    <input type="date" id="ftglpembuatan" name="ftglpembuatan" max="2019-03-10" placeholder="Tanggal Pembuatan..">
                    <label for="ftgl">Tgl Kadaluwarsa <span class="note"> Tanggal Min 10 Maret 2019 </span></label>
                    <input type="date" id="ftglkadaluwarsa" name="ftglkadaluwarsa" min="2019-03-10" placeholder="Tanggal Kadaluwarsa..">
                    <label for="ftgl">Telephon Toko</label>
                    <input type="tel" id="ftel" name="ftel" pattern="[0-9]{7,12}" placeholder="Harus 7-12 angka !!!">

                    <label for="ftgl">Email Toko</label>
                    <input type="email" id="femail" name="femail" placeholder="Harus menggunakan @">

                    <label for="frak">Rak Obat</label>
                    <input list="rakobat" name="frakobat" placeholder="RAK Penyimpanan">
                    <datalist id="rakobat">
                    <option value="RAK NO 1">
                    <option value="RAK NO 2">
                    <option value="RAK NO 3">
                    <option value="RAK NO 4">
                    <option value="RAK NO 5">
                    </datalist>
                    <input type="submit" value="Submit">
                    </form>
                </div>
            </div>
            <div class="kanan">
                <h2>Halaman Judul</h2>
                <p>kontent</p>
            </div>
        </article>
    </section>
    <footer>
        <strong>17.5.00137 || Wahyu Setiawan</strong> 
    </footer>
</body>
</html>

