<%-- 
    Document   : hasil-form
    Created on : Mar 27, 2019, 4:04:59 AM
    Author     : abah
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="css/cssku.css"> 
</head>
<body>
    <header>
        <img src="image/wordmark4.png" alt="none">
    </header>
    <nav class="menu">
        <aside class="daftar-menu">
            <ul>
                <li><a href="/WA-GlassFishServer/">Home</a></li>
                <li><a href="/WA-GlassFishServer/form.jsp">Form</a></li>
                <li><a href="#contact">Contact</a></li>
                <li><a href="#about">About</a></li>
            </ul>              
        </aside>
    </nav>
    <section class="isi">
        <article class="konten">
            <div class="kiri">
                <h2>Halaman Judul</h2>
                <div class="tengah-medium">
                    <div class="box-container text-kiri">
                        <label for="foobat">Nama Obat : </label>
                        <span class="form-output"> <%= request.getParameter("fobat")%> </span>
                        <label for="fotglpembuatan">Tgl Pembuatan : </label>
                        <span class="form-output"> <%= request.getParameter("ftglpembuatan")%> </span>
                        <label for="fotglkadaluwarsa">Tgl Kadaluwarsa : </label>
                        <span class="form-output"> <%= request.getParameter("ftglkadaluwarsa")%> </span>
                        <label for="fotel">Telephon Toko : </label>
                        <span class="form-output"> <%= request.getParameter("ftel")%> </span>
                        <label for="foemail">Email Toko : </label>
                        <span class="form-output"> <%= request.getParameter("femail")%> </span>
                        <label for="forak">Rak : </label>
                        <span class="form-output"> <%= request.getParameter("frakobat")%> </span>
                        <a href="/WA-GlassFishServer/form.jsp"><button>Back</button></a>
                    </div>
                </div>
            </div>
            <div class="kanan">
                <h2>Halaman Judul</h2>
                <p>kontent</p>
            </div>
        </article>
    </section>
    <footer>
        <strong>17.5.00137 || Wahyu Setiawan</strong> 
    </footer>
</body>
</html>
